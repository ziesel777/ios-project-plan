import $ from 'jquery';
import dragscroll from 'dragscroll';

import store from '@store';

import BigPanel from './big-panel/BigPanel.vue';
import UploadFile from './upload-file/UploadFile.vue';
import ContentResizable from './content-resizable/ContentResizable.vue';

import net from '../core/net';


class Draw {
	constructor(el='.js-plan'){
		let $plan = this.$plan = $(el);
		let offset = this.offset = $plan.offset();
		let outerHeight = this.outerHeight = $plan.outerHeight();
	}
	scrollToElem(time=500){
		// let elem = this.$plans.get(store.state.newPageId);
		let draw = this;

		$('html, body').animate({
			// scrollTop: $(elem).offset().top
			scrollTop: draw.offset.top,

		}, {
			duration: time,
			// easing: "linear" // по умолчанию «swing»
		});
	}
	dragscroll(){
		let el = null;

		$(document).off();
		$(document).mousedown(function (e) {
			let target = $(e.target).closest('[contenteditable]')[0];
			if (e.button == 0) {
				$('#app').css({'cursor':'grabbing'});
			}
			if(el && target!=el){
				$(el).blur();
			}
		});
		$(document).mouseup(function (e) {
			$('#app').css({'cursor':'default'});
		});

		$('[contenteditable]').off()
			.click(function (e) {
				el = this;
			})
			.mouseenter(function (e) {
				if($(e.target).closest('[contenteditable]').attr('contenteditable')=='true'){
					// console.log(e);
					$('body').removeClass('dragscroll');
					dragscroll.reset();
				}
			})
			.mouseleave(function (e) {
				if($(e.target).closest('[contenteditable]').attr('contenteditable')=='true'){
					// console.log(e);
					$('body').addClass('dragscroll');
					dragscroll.reset();
				}
			});
	}
}

export default {
	name:'Draw',
	props:{
		data: Object,
		pageId: Number,
		editmode: Boolean,
	},
	components:{
		'big-panel': BigPanel,
		'upload-file': UploadFile,
		'content-resizable': ContentResizable,
	},
	data() {
		return {
			message: 'Здесь должен быть чертеж или текст',
			pages: store.state.pages,
			project: store.state.project,
			uploadImage: [],
		};
	},
	computed: {
		year(){
			let date = new Date();
			return date.getFullYear();
		},
		page(){
			return this.pages[this.pageId];
		}
	},
	methods: {
		setContent(key,e){
			let data = this.project;
			let content = e.target.textContent;

			if(key=='title'){
				data = this.pages[this.pageId];
			}
			if(key=='text'){
				data = this.pages[this.pageId].content;
				content = $(e.target).html();
			}
			data[key] = content;
		},
		removePage(){
			store.commit('showModal',{
				action: 'removePage',
				data:{ pageId: this.pageId }
			});
		},
	},
	mounted() {
		let draw = new Draw(this.$el);
		this.page.options['offset'] = draw.offset;
		this.page.options['outerHeight'] = draw.outerHeight;

		if(store.state.isAddPage){
			draw.scrollToElem();
			store.state.isAddPage = false;
		}

		draw.dragscroll();
	},
	created() {
		if(this.page.options.type=='default-page' && !this.page.content['textBlocks']){
			this.page.content['textBlocks']=[];
		}
		// console.log(this.page);

	},
};