// import $ from 'jquery';
import hash from 'hash-generator';

import store from '@store';
import ImageResizable from '../image-resizable/ImageResizable.vue';
import TextBlock from '../text-block/TextBlock.vue';

// import html2canvas from 'html2canvas';
// import {jsPDF} from 'jspdf';

export default {
	// name:'ContentImages',
	props:{
		pageId: Number,
	},
	components:{
		'image-resizable': ImageResizable,
		'text-block': TextBlock,
	},
	data(){
		return {
			page: store.state.pages[this.pageId],
		};
	},
	computed: {
		editmode(){
			return store.state.editmode;
		},
		images(){
			return this.page.content.img;
		},
		blocks(){
			return this.page.content.textBlocks;
		}
	},
	mounted() {
		// let page = this.page;
		// console.log(page);
		// let el = $('.plan__big-panel')[0];
		// let el = this.$el;
		// console.log(el);

		// let doc = new jsPDF('p','pt','a4');

		// doc.addHTML(el);
		// doc.save('html.pdf');

		// doc.html(el, {
		// 	callback: function (doc) {
		// 	  doc.save('html.pdf');
		// 	}
		// });

		// html2canvas(el,{
		// 	scale: 3,
		// }).then(function(canvas) {
		// 	let imgData = canvas.toDataURL('image/png');
		// 	let doc = new jsPDF();

		// 	doc.addImage(imgData, 'PNG', 0, 0);
		// 	doc.save('sample.pdf');
		// });
	},
	methods: {
		hash(){
			let code = hash(4);
			console.log(code);
			return code;
		}
	},
	watch: {
	}
};