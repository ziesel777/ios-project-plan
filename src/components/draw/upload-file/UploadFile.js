import $ from 'jquery';
import net from '../../core/net';
import store from '../../../store/store';

class UploadFile{
	constructor(el){
		this.$el = $(el);
	}
	init(){
		let dropZone = this.$el;

		// dropZone.on('drag dragstart dragend dragover dragenter dragleave drop', function(){
		// 	return false;
		// });

		dropZone.on('dragover dragenter', function() {
			dropZone.addClass('dragover');
		});
		dropZone.on('dragleave', function(e) {
			dropZone.removeClass('dragover');
		});
		dropZone.on('drop', function(e) {
			dropZone.removeClass('dragover');

			// let files = e.originalEvent.dataTransfer.files;
			// sendFiles(files);
			// console.log(files);
		});
	}
}

export default {
	name:'UploadFile',
	props :{
		image: Object,
		pageId: Number,
	},
	data(){
		return {
			id: `id-upload-file-${this._uid}`,
			page: store.state.pages[this.pageId],
		};
	},
	mounted() {
		let uploadFile = new UploadFile(this.$el);
		uploadFile.init();
	},
	methods: {
		async uploadFile(e){
			let control = e.target;
			let file = control.files;
			let formData = new FormData();

			formData.append('file',file[0]);
			formData.append('house','test');
			formData.append('project','test');


			console.log(file);

			if(file.length>0){
				let res = await net.uploadFile({
					controller: 'floor',
					action: 'saveFile',
					data:formData
				});

				let data = res.data;
				console.log(res);
				console.log(data.UF_FILES);

				if(res.status == 'ADD_FILE_INFO_SUCCESS'){
					let image = this.image;
					let id = Math.max(...Object.keys(data.UF_FILES));
					let src = data.UF_FILES[id];

					if(image){
						image.id = id;
						image.src = src;
					} else {
						let images = this.page.content.img;

						image = Object.assign({},store.state.emptyImage);

						image.id = id;
						image.src = src;

						images.push(image);
					}
				} else {
					console.error(data.errorMessage);
				}
			}
		},
		addImgGallery(){
			let win = store.state.galleryWin;
			win.show = true;
			win.pageId = this.pageId;
			if(this.image){
				win.image = this.image;
			}
		},
	},
};