import $ from 'jquery';
import dragscroll from 'dragscroll';

// import UploadFile from '../upload-file/UploadFile.vue';
import VueDraggableResizable from 'vue-draggable-resizable';
import 'vue-draggable-resizable/dist/VueDraggableResizable.css';

import store from '@store';

export default {
	name:'text-block',
	props:{
		pageId: Number,
		textIndex: Number,
		text: Object,
	},
	components:{
		'vue-draggable-resizable': VueDraggableResizable,
		// 'upload-file': UploadFile,
	},
	data() {
		return {
			id: this.text.id,
			width: this.text.width? this.text.width : 450,
			height: this.text.height?this.text.height : 200,
			x: this.text.x!==''?this.text.x : 95,
			y: this.text.y!==''?this.text.y : 100,

			editable: false,
		}
	},
	computed: {
		editmode(){
			return store.state.editmode;
		},
		// imgData(){
		// 	let page = store.state.pages[this.pageId];
		// 	let blocks = page.content.textBlocks;
		// 	let text = blocks[this.textIndex];

		// 	return text;
		// },
	},
	methods: {
		onResize(x, y, width, height) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		},
		onDrag(x, y) {
			this.x = x;
			this.y = y;

			$('body').removeClass('dragscroll');
			dragscroll.reset();
		},

		onDragstop() {
			$('body').addClass('dragscroll');
			dragscroll.reset();

			this.setTextBlock();
		},
		onResizestop(){
			this.setTextBlock();
		},

		setTextBlock(){
			let text = this.text;

			text.x = this.x;
			text.y = this.y;
			text.height = this.height;
			text.width = this.width;
		},

		delTextBlock(){
			let page = store.state.pages[this.pageId];
			let blocks = page.content.textBlocks;

			let text = blocks.splice(this.textIndex,1)[0];
			console.log(text);
		},
		editBlockBlur(e){
			setTimeout(() => {
				if(this.editable) this.editable = false;
			}, 100);
			this.text.text = $(e.target).html();
		}
	},
}