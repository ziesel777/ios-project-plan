import $ from 'jquery';
import dragscroll from 'dragscroll';

import UploadFile from '../upload-file/UploadFile.vue';
import VueDraggableResizable from 'vue-draggable-resizable';
import 'vue-draggable-resizable/dist/VueDraggableResizable.css';

import store from '@store';

export default {
	name:'ImageResizable',
	props:{
		pageId: Number,
		imgIndex: Number,
		image: Object,
	},
	components:{
		'vue-draggable-resizable': VueDraggableResizable,
		'upload-file': UploadFile,
	},
	computed: {
		editmode(){
			return store.state.editmode;
		},
		imgData(){
			let page = store.state.pages[this.pageId];
			let images = page.content.img;
			let image = images[this.imgIndex];

			return image;
		},
	},
	data(){
		return {
			id: this.image.id,
			width: this.image.width? this.image.width : 450,
			height: this.image.height?this.image.height : 200,
			x: this.image.x!==''?this.image.x : 95,
			y: this.image.y!==''?this.image.y : 100
		};
	},
	mounted() {

	},
	methods: {
		onResize(x, y, width, height) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		},
		onDrag(x, y) {
			this.x = x;
			this.y = y;

			$('body').removeClass('dragscroll');
			dragscroll.reset();
		},

		onDragstop() {
			$('body').addClass('dragscroll');
			dragscroll.reset();

			this.setImage();
		},
		onResizestop(){
			this.setImage();
		},

		setImage(){
			let img = this.imgData;

			img.x = this.x;
			img.y = this.y;
			img.height = this.height;
			img.width = this.width;
		},
		delImage(){
			// store.commit('showModal',{
			// 	action: 'delImage',
			// 	data:{
			// 		imgIndex: this.imgIndex,
			// 		pageId: this.pageId
			// 	}
			// });
			let page = store.state.pages[this.pageId];
			let images = page.content.img;

			let img = images.splice(this.imgIndex,1)[0];
			console.log(img);
		},
		addImage(){
			// let page = store.state.pages[this.pageId];
			// let images = page.content.img;
			// let emptyImage = Object.assign({},store.state.emptyImage);

			// images.push(emptyImage);
			store.commit('addImage',{pageId: this.pageId});
		},
	},
};
