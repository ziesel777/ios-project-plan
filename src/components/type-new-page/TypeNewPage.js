import $ from 'jquery';
import store from '../../store/store';
import dragscroll from 'dragscroll';


export default {
	name:'TypeNewPage',
	data(){
		return{
			options:{
				type:'default-page',
				format: 'vertical', // 'horizontal'
				typeContent: 'draw', // 'text'
			},
			idWinActive:0,
			typeWin:[
				'format',
				// 'content',
				'insert'
			],
			win:{
				format:true,
				// content:false,
				insert:false,
			},
			btn:{
				back:false,
				next:true,
				cancel:true,
			},
			indexPage: store.state.pages.length - 1,
		};
	},
	computed: {
		getPages(){
			return store.state.pages;
		},
	},
	methods: {
		setFormat(format) {
			this.options.format = format;
		},
		setContent(typeContent) {
			this.options.typeContent = typeContent;
		},
		next(n){
			this.idWinActive += n;
			if(this.idWinActive < this.typeWin.length){
				this.stateComponent();
			} else {
				this.addPage();
				this.close();
			}
		},
		stateComponent(){
			let type = this.typeWin[this.idWinActive];

			this.btn.back=(this.idWinActive>0);
			this.btn.cancel=(this.idWinActive==0);

			for (const key in this.win) {
				this.win[key]=(key==type);
			}
		},
		addPage(){
			let options = $.extend(true, {}, this.options);
			let page = $.extend(true, {}, store.state.emptyPage);
			page.options = options;
			console.log(page);
			store.commit('addPage', {page:page,id:Number(this.indexPage)});
		},
		close(){
			this.options = {
				format: 'vertical', // 'horizontal'
				typeContent: 'draw', // 'text'
			};
			this.idWinActive = 0;
			store.state.tpwin.show = false;
		},
		exit(){
			this.idWinActive = 0;
			store.state.tpwin.show = false;
		},
		rangeIndexAddPage(min,max){
			if(this.indexPage<min) this.indexPage = min;
			if(this.indexPage>max) this.indexPage = max;
		}
	},
	updated() {
		let $btnGroup = $('.js-type-page').find('.js-btn-group');
		let $btn = $btnGroup.find('.js-app-btn');

		if(!$btn.hasClass('cancel')){
			$btnGroup.css({
				'justify-content': 'space-between',
			});
		} else {
			$btnGroup.attr('style','');
		}

		this.rangeIndexAddPage(2,this.getPages.length-1);
	},
	mounted () {
		$('body').removeClass('dragscroll');
		dragscroll.reset();
	},
	destroyed () {
		$('body').addClass('dragscroll');
		dragscroll.reset();
	},
};