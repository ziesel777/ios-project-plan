import $ from 'jquery';
import store from '../../store/store';

class SideBar {
	constructor(){
		this.$sidebar = $('.js-side-bar');
		this.$btn = this.$sidebar.find('.js-btn-toggle');
		this.$li = this.$sidebar.find('li');
		this.$close = this.$sidebar.find('.js-btn-close');
	}
	init(){
		const t = this;

		t.$btn.click(function () {
			t.$sidebar.addClass('show');
			$(this).stop().hide(0);
			t.$close.show();
		});

		t.$close.click(function () {
			t.hideSideBar();
		});

		t.$sidebar.click(function () {
			if(t.timer){
				clearTimeout(t.timer);
			}
		});

		// нажатие по открытой области вне сайдбара
		$('#app').click(function (e) {
			if($(e.target).closest('.js-side-bar').length==0 && t.$sidebar.hasClass('show')){
				if(t.timer){
					clearTimeout(t.timer);
				}
				t.timer = setTimeout(() => {
					t.hideSideBar();
				}, 5000);
			}
		});
	}
	hideSideBar(time=500){
		this.$sidebar.removeClass('show');
		this.$btn.show(500);
		this.$close.hide();
	}
}

export default {
	name:'SideBar',
	props:{
		editmode: Boolean,
		zoommode: Boolean,
		addNewPage: Function,
		onEditMode: Function,
		onZoomMode: Function,
		printProject: Function,
	},
	data(){
		return {
			// listMenu:[
			// 	{
			// 		title:'Новый лист',
			// 		ico:'far fa-file',
			// 		action: 'addNewPage'
			// 	},
			// 	{
			// 		title:'Редактировать',
			// 		ico:'far fa-edit',
			// 		action: ''
			// 	},
			// 	{
			// 		title:'Очистить проект',
			// 		ico:'fas fa-eraser',
			// 		action: 'clearDraws'
			// 	},
			// ]
		};
	},
	methods:{
		clearDraws(){
			let mwin = store.state.mwin;

			mwin.show = true;
			mwin.action = 'clearDraws';
		},
		saveProject(){
			let mwin = store.state.mwin;

			mwin.show = true;
			mwin.action = 'saveProject';
		}
	},
	mounted(){
		let sidebar = new SideBar();
		sidebar.init();
	}
};