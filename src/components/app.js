import $ from 'jquery';
// import dragscroll from 'dragscroll';
import net from './core/net';
import store from '@store';
import MediumEditor from 'medium-editor';

import PreMenu from './pre-menu/PreMenu.vue';
import SideBar from './side-bar/SideBar.vue';
import TopMenu from './top-menu/TopMenu.vue';
import ModalWindow from './modal-window/ModalWindow.vue';
import TypeNewPage from './type-new-page/TypeNewPage.vue';
import ImageGallery from './image-gallery/ImageGallery.vue';

import Draw from './draw/draw.vue';

$(window).keydown(function (e) {
	if (e.ctrlKey && e.keyCode == 13) {
	  location.reload(true);
	}
});

export default {
	components:{
		'pre-menu': PreMenu,
		'side-bar': SideBar,
		'top-menu': TopMenu,
		'modal-window': ModalWindow,
		'type-new-page': TypeNewPage,
		'image-gallery': ImageGallery,
		'draw': Draw,
	},
	data() {
		return {
			editmode: store.state.editmode,
			zoommode: store.state.zoommode,
			// draws: store.state.draws,
			pages: store.state.pages,
			tpwin: store.state.tpwin,
			galleryWin: store.state.galleryWin,
		};
	},
	computed: {
		getpage(){
			return this.pages.length;
		},
		isMWShow(){
			return store.state.mwin.show;
		}
	},
	methods: {
		addPage() {
			this.tpwin.show=true;
		},
		onEditMode() {
			this.editmode = !this.editmode;
			store.state.editmode = this.editmode;
		},
		isEditMode(){
			if(this.pages.length==0){
				this.editmode = false;
			}
		},
		onZoomMode(){
			this.zoommode = !this.zoommode;
			store.state.zoommode = this.zoommode;
		},
		printProject(){
			this.editmode = false;
			this.zoommode = false;
			store.state.editmode=false;
			store.state.zoommode=false;

			setTimeout(() => {
				window.print();
			}, 300);
		},
		getListImage(){
			let imgList = [];
			let pages = this.pages;

			for(let page of pages){
				let images = page.content.img;
				if(images){
					imgList.push(...images);
				}
			}
			return imgList;
		},
		async syncProjectImages(){
			let projImages = this.getListImage();
			let idHouse = store.state.idHouse;
			let idProject = store.state.idProject;
			let res = await net.ajax({
				controller: 'floor',
				action:'getFiles',
				data:{
					house: idHouse,
					project: idProject,
				}
			});


			if(res.status=="GET_FILES_SUCCESS"){
				let servImages = res.data;
				let check = true;
				// console.log(projImages);
				// console.log(servImages);

				console.log('Синхронизация изображений:');
				for(let id in servImages){
					check = true;
					for(let img of projImages){
						if(img.id == id){
							// console.log(id);
							check = false;
							break;
						}
					}
					if(check){
						net.ajax({
							controller:'floor',
							action:'deleteFile',
							data: {
								house: idHouse,
								project: idProject,
								file: id
							}
						});
						console.log('Неиспользуемый файл удален id',id);
					}
				}
			}
		},
	},
	created() {
		if(net.hostname==='localhost'){
			Object.assign(store.state.project,store.state.emptyData.project);
			store.state.pages.push(...store.state.emptyData.pages);
		} else {
			store.commit('getProject');
		}

		// this.syncProjectImages();
	},
	mounted () {
		if(this.editmode){
			setTimeout(() => {
				let editor = new MediumEditor('.editable',{
					toolbar: false
				});
			}, 300);
		}
	},
	updated() {
		this.isEditMode();
		store.commit('autozoom');

		// if(this.getpage==0){
		// 	$('body').removeClass('dragscroll');
		// 	dragscroll.reset();
		// } else {
		// 	$('body').addClass('dragscroll');
		// 	dragscroll.reset();
		// }
	},
	watch: {
		editmode(new_data,old_data){
			if(new_data){
				let editor = new MediumEditor('.editable',{
					toolbar: false
				});
			}
		}
	},
};