import store from "../../store/store";

export default {
	name:'ModalWindow',
	data(){
		return{
			mwin: store.state.mwin,
		};
	},
	computed:{
		type(){
			// let mwin = this.mwin;
			let data = {
				'removePage':{
					ico: 'fas fa-exclamation-triangle',
					title: 'Внимание!',
					info: `Вы действительно хотите удалить ${this.mwin.data.pageId} лист?`,
				},
				'clearDraws':{
					ico: 'fas fa-exclamation-triangle',
					title: 'Внимание!',
					info: 'Вы действительно хотите очистить проект?',
				},
				'saveProject':{
					ico: 'far fa-save',
					title: 'Внимание!',
					info: 'Вы действительно хотите сохранить изменения в проекте?',
				},
				'delImage':{
					ico: 'fas fa-exclamation-triangle',
					title: 'Внимание!',
					info: 'Вы действительно хотите удалить изображение?',
				},
			};
			return data[this.mwin.action];
		},
	},
	methods:{
		mwAction(confrim){
			store.dispatch('mwUserConfirm',confrim);
		}
	}
};