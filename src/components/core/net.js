import $ from 'jquery';
import hash from 'hash-generator';

class Net{
	constructor(){
		this.url = window.location.href;
		this.hostname = window.location.hostname;
		this.domain = this.getDomain();
		this.params = this.getParams();
		// console.log(this.domain);
	}

	getDomain(){
		let path = '';

		// const href = window.location.href;
		const protocol = window.location.protocol;
		const hostname = window.location.hostname;
		const port = window.location.port;

		path = `${protocol}//${hostname}`;

		if(port){
			path += `:${port}`;
		}

		return path;
	}

	getParams(){
		const url = new URL(this.url);
		let params = {};

		for(let [name,value] of url.searchParams){
			params[name]=value;
		}

		return params;
	}

	/**
		?controller=floor
		action=savePrint / getPrint / deletePrint

		всегда передаем 2 параметра (они будут тебе в урле переходить):
		house
		project

		в случае savePrint добавляется еще ключ info - куда складываешь json-строку с нужными тебе данными
	*/
	ajax({controller,action,data,params,path}){
		let id = hash(4);
		let url = path?path:'';

		url += `/ajax/?controller=${controller}&action=${action}`;
		let message = `${id}-${controller}-${action}->`;


		if(params){
			for(let key in params){
				let val = params[key];
				url+=`${key}=${val}`;
			}
		}

		console.time(message);
		return new Promise(resolve => {
			$.ajax({
				type: "POST",
				url,
				data: data? data:'',
				// headers: {
				// 	"Authorization": "Basic " + btoa('webdav_user:thethek8')
				// },
				dataType: "json",
				success: function (response) {
					console.timeEnd(message);

					resolve(response);
				}
			});
		});
	}

	uploadFile({controller,action,data,path}){
		let id = hash(4);
		// const data = opt.data;
		let url = path?path:'';
		let message = `${id}-${controller}-${action}->`;

		url += `/ajax/?controller=${controller}&action=${action}`;

		console.time(message);
		return new Promise(resolve=>{
			$.ajax({
				type: 'POST',
				url,
				data,
				// headers: {
				// 	"Authorization": "Basic " + btoa('webdav_user:thethek8')
				// },
				async: false,
				cache: false,
				contentType: false,
				processData: false,
				success: function (res) {
					console.timeEnd(message);

					resolve(res);
				}
			});
		});
	}

}

export default new Net();
