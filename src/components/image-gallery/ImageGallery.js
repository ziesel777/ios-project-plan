import net from '../core/net';
import store from '@store';

export default {
	data() {
		return {
			galleryWin: store.state.galleryWin,
			images:[],
			curImg:'',
			pages: store.state.pages,
		}
	},
	methods: {
		close(){
			this.galleryWin.show = false;
			this.curImg = '';
		},
		addImg(){
			let pageId = this.galleryWin.pageId;
			let curImg = this.curImg;

			if(curImg && pageId){
				let page = this.pages[pageId];
				let images = page.content.img;

				if(this.galleryWin.image){
					let image = this.galleryWin.image;
					image.id = curImg.id;
					image.src = curImg.src;

					this.galleryWin.image='';
				} else {
					images.push(curImg);
				}

				this.galleryWin.show = false;
			}
		},
		checkImg(img){
			// console.log(img);
			this.curImg = img;
		},
		async getImages(){
			let idHouse = store.state.idHouse;
			let idProject = store.state.idProject;
			let res = await net.ajax({
				controller: 'floor',
				action:'getFiles',
				data:{
					house: idHouse,
					project: idProject,
				}
			});

			if(!res.data.errorMessage){
				let data = res.data;
				// let svgsrc = `/upload/projects/${idHouse}--${idProject}.svg`;
				let svgsrc = `/upload/projects/test.jpg`;

				let img = new Image();
				img.src = svgsrc;
				img.onload = ()=>{
					let image = Object.assign({},store.state.emptyImage);
					image.id = 'svg';
					image.src = svgsrc;
					this.images.push(image);
				};
				img.onerror = function(){
					console.log(`SVG файла проекта не существует:`,svgsrc);
				};

				for(let id in data){
					let src = data[id];
					let image = Object.assign({},store.state.emptyImage);
					if(src){
						image.id = id;
						image.src = src;
						this.images.push(image);
					}
				}
				console.log(this.images);
			} else {
				console.log(res.data.errorMessage);
			}
		},
	},
	async created() {
		this.getImages();
		console.log(this.galleryWin.pageId);
	},
}