import $ from 'jquery';
import store from '@store';

export default {
	name:'TopMenu',
	props:{
		page:Number,
		editmode:Boolean,
		zoommode:Boolean,
	},
	data(){
		return{
			scrollTop: 0,
			curPage: 1,
		};
	},
	computed:{
		// getEditMode(){
		// 	return this.editmode? 'вкл. режим ред.':'';
		// },
		pages(){
			return store.state.pages;
		},
	},
	methods: {
		getScrollTop(){
			this.scrollTop = $(window).scrollTop();
		},
		getCurPage(scrollTop){
			let curPage = this.curPage;
			let pages = this.pages;
			for(let [index,page] of pages.entries()){
				let offsetTop = page.options.offset.top;
				let height = page.options.outerHeight;

				if(
					scrollTop > (offsetTop - height*1/3) &&
					scrollTop < (offsetTop + height*1/3) &&
					curPage!=(index+1)
					){
					curPage = index+1;
				}
			}
			return curPage;
		},
		showMenuBar(){
			let show = false;
			let page = this.pages[this.curPage-1];

			if(page && page.options.type=='default-page') show = true;

			let $menuBar = $(this.$el).find('.js-menu-bar');
			if(show){
				$menuBar.fadeIn({
					start(){
						$(this).css({'display':'flex'});
					}
				});
			} else {
				$menuBar.fadeOut();
			}

			return show;
		},
		addImage(){
			store.commit('addImage',{
				pageId: this.curPage-1,
			});
		},
		addTextBlock(){
			store.commit('addTextBlock',{
				pageId: this.curPage-1,
			});
		}
	},
	created() {
		let app = this;

		$(window).on('scroll', function () {
			app.getScrollTop();
			// console.log(app.scrollTop);
		});
	},
	watch: {
		scrollTop(val){
			let scrollTop = val;
			if(scrollTop>=50){
				this.curPage  = this.getCurPage(scrollTop);
			} else {
				this.curPage = 1;
			}
			this.showMenuBar();
		},
	}
};