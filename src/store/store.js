import $ from 'jquery';
import Vue from 'vue';
import Vuex from 'vuex';

import net from '@comp/core/net';


Vue.use(Vuex);

const idHouse = net.params.house?net.params.house:'test';
const idProject = net.params.project?net.params.project:'test';

let drawData = {
	project:{
		number: 'ис-1-174078-19-со',
		nameGroup: 'СЕГМЕНТ СЕТИ ДОСТУПА ООО фирма «ИНТЕРСВЯЗЬ» г.Челябинск, ул.Барбюса, д.144-А',
		typeBuild: 'Размещение телекомуникационного оборудования',
		stage:'РД',

		company:'ООО фирма "ИНТЕРСВЯЗЬ"',
		develop: 'Квасницын',
		verification: 'Юдин',
		t_contr:'',
		n_contr:'',
		gip:'Радющевский',
	},

	pages:[
		{
			title: 'Титульный лист',
			content:{
				img:'',
				text:'',
			},
			options:{
				type:'title-page',
				format: 'vertical', // 'horizontal'
				typeContent: 'draw', // 'text'
			}
		},
		{
			title: 'Ведомость чертежей и документов',
			content:{
				img:'',
				text:'',
			},
			options:{
				type:'contents-page',
				format: 'vertical', // 'horizontal'
				typeContent: 'draw', // 'text'
			}
		},
		{
			title: 'Общие данные',
			content:{
				img: '',
				text: '1 ОБЩИЕ ДАННЫЕ Проектом предусмотрено размещение телекоммуникационного оборудования ООО фирма «Интерсвязь» в жилом доме № 144-А по ул. Барбюса (60 квартир) в г. Челябинск. 1.1 Прокладка волоконно-оптического кабеля связи Волоконно-оптический кабель связи (ВОК) ОКГС-16 до подключаемого жилого дома прокладывается воздушно-подвесным способом от жилого дома № 144-Б по ул. Барбюса (3 подъезд, 1/2 этаж). В процессе монтажа ВОК исключить возможность размещения ОК перед окнами жилых квартир. ВОК крепится крепится за стену надкровельной будки второго подъезда натяжным спиральным зажимом. Далее выполняется ввод волоконно-оптического кабеля через стену надкровельной будки в ПНД трубе d25мм под углом 5-10 ° к внешней стене, выполняется гидроизоляция отверстия. Далее вдоль стен надкровельной будки ВОК спускается до уровня верхнего этажа жилого дома и прокладывается под потолком верхнего этажа в гофрированной ПВХ трубе d20 до проектируемого коммутационного шкафа на межэтажной площадке 4/5 этажей (см. лист 9).',
			},
			options:{
				type:'description-page',
				format: 'vertical', // 'horizontal'
				typeContent: 'text', // 'text'
			}
		},
	],
};

let emptyData = {
	project:{
		number: 'хх-0-000000-00-хх',
		nameGroup: 'НАЗАВАНИЕ ООО фирма «ИНТЕРСВЯЗЬ» адрес',
		typeBuild: 'Размещение оборудования',
		stage:'РД',

		company:'ООО фирма "ИНТЕРСВЯЗЬ"',
		develop: '',
		verification: '',
		t_contr:'',
		n_contr:'',
		gip:'',
	},

	pages:[
		{
			title: 'Титульный лист',
			content:{
				img:'',
				text:'',
			},
			options:{
				type:'title-page',
				format: 'vertical', // 'horizontal'
				typeContent: 'draw', // 'text'
			}
		},
		{
			title: 'Ведомость чертежей и документов',
			content:{
				img:'',
				text:'',
			},
			options:{
				type:'contents-page',
				format: 'vertical', // 'horizontal'
				typeContent: 'draw', // 'text'
			}
		},
		{
			title: 'Общие данные',
			content:{
				img: '',
				text: 'Текст общих данных',
			},
			options:{
				type:'description-page',
				format: 'vertical', // 'horizontal'
				typeContent: 'text', // 'text'
			}
		},
	],
};

export default new Vuex.Store({
	state:{
		idHouse,
		idProject,
		emptyData,
		emptyImage: {
			id: '',
			src: '',
			x:'',
			y:'',
			width: '',
			height: '',
		},
		emptyTextBlock:{
			id: '',
			text: '',
			x:'',
			y:'',
			width: '',
			height: '',
		},
		emptyPage: {
			title: '',
			content:{
				img:[],
				text:'',
				textBlocks:[],
			},
			options:{
				type:'default-page',
				format: 'vertical', // 'horizontal'
				typeContent: 'draw', // 'text'
			}
		},

		// project: drawData.project,
		// pages:[...drawData.pages],
		// pages:[
		// 	{
		// 		title: 'Тестовая страница',
		// 		content:{
		// 			img:[
		// 				{
		// 					id: 2,
		// 					src: './assets/img/draw.png',
		// 					x:0,
		// 					y:0,
		// 					width: 250,
		// 					height: 150,
		// 				},
		// 				{
		// 					id: '',
		// 					src: '',
		// 					x:'',
		// 					y:'',
		// 					width: '',
		// 					height: '',
		// 				},
		// 			],

		// 			// img:[],
		// 			text:'',
		// 		},
		// 		options:{
		// 			type:'default-page',
		// 			format: 'vertical', // 'horizontal'
		// 			typeContent: 'draw', // 'text'
		// 		}
		// 	},
		// ],

		project:{},
		pages:[],

		newPageId:0,		// счетчик id новодобавленной страницы в массив

		editmode: true,
		zoommode: false,
		isAddPage: false,

		// модальное окно
		mwin:{
			show: false,
			action:'',
			data: {},
		},

		// окно выбора типа добавляемой страницы
		tpwin:{
			show: false,
		},
		galleryWin:{
			show: false,
			pageId:'',
			image:'',
		}
	},
	mutations:{
		addPage(state, data){
			if(data.id == state.pages.length-1){
				state.pages.push(data.page);
			} else {
				state.pages.splice((data.id+1),0,data.page);
			}
			state.newPageId = data.id+1;
			// console.log(state.newPageId);
			state.isAddPage = true;
		},
		removePage(state){
			let pages = state.pages;
			let pageId = state.mwin.data.pageId;

			pages.splice(pageId,1);
		},
		clearDraws(state){
			state.pages.splice(3,state.pages.length-2);
		},
		async delImage(state){
			// let pageId = state.mwin.data.pageId;
			// let imgIndex = state.mwin.data.imgIndex;
			// let page = state.pages[pageId];
			// let images = page.content.img;

			// let img = images.splice(imgIndex,1)[0];
			// console.log(img);
			// this.commit('saveProject');



			// let filterImages = state.pages.filter(page=>{
			// 	let images = page.content.img;
			// 	for(let image of images){
			// 		return image.id == img.id;
			// 	}
			// });

			// if(filterImages.length==0 && img.id){
			// 	const data = {
			// 		project: state.project,
			// 		pages: state.pages,
			// 	};

			// 	const res = await net.ajax({
			// 		controller:'floor',
			// 		action:'deleteFile',
			// 		data: {
			// 			house: idHouse,
			// 			project: idProject,
			// 			file: img.id
			// 		}
			// 	});

			// 	if(res.status === 'DELETE_FILE_INFO_SUCCESS'){
			// 		console.log('изображение успешно удалено');
			// 		this.commit('saveProject');
			// 	} else {
			// 		console.error(res.data.errorMessage);
			// 	}

			// 	console.log(res);
			// }
		},
		addImage(state,{pageId}){
			let page = state.pages[pageId];
			let images = page.content.img;
			let emptyImage = Object.assign({},state.emptyImage);

			if(images.length==0) images.push(emptyImage);
			else if(images[images.length-1].id){
				images.push(emptyImage);
			}

			// images.push(emptyImage);
		},
		addTextBlock(state,{pageId}){
			let page = state.pages[pageId];
			let blocks = page.content.textBlocks;
			// console.log(page);
			let emptyTextBlock = Object.assign({},state.emptyTextBlock);

			blocks.push(emptyTextBlock);
		},
		showModal(state,payload){
			let mwin = state.mwin;

			mwin.show = true;
			mwin.action = payload.action;
			mwin.data = payload.data;
		},
		autozoom(state){
			if(state.zoommode){
				let drawWidth = $('.js-draw-wrapper').width();
				let winWidth = $(window).width();

				if(winWidth < drawWidth){
					let scale = winWidth/drawWidth;
					$('.js-draw-wrapper').css({
						'zoom':`${scale*90}%`,
						// '-moz-transform': `scale(${scale*0.8})`,
						// '-moz-transform-origin': '0 0',
						// 'transform':`scale(${scale})`
					});
				}
			} else {
				$('.js-draw-wrapper').attr('style','');
			}
		},

		async saveProject(state){
			const data = {
				project: state.project,
				pages: state.pages,
			};

			const res = await net.ajax({
				controller:'floor',
				action:'savePrint',
				data: {
					house: idHouse,
					project: idProject,
					info: JSON.stringify(data)
				}
			});

			if(res.status === 'ADD_PRINT_INFO_SUCCESS'){
				console.log('данные проекта сохранены');
			} else {
				console.log('Ошибка! Что-то пошло не так');
			}

			console.log(res);

		},

		async getProject(state){
			const res = await net.ajax({
				controller:'floor',
				action:'getPrint',
				data: {
					house: idHouse,
					project: idProject,
				},
			});

			if(res.status === 'GET_PRINT_INFO_SUCCESS' && res.data){
				let data = JSON.parse(res.data);

				Object.assign(state.project,data.project);
				state.pages.push(...data.pages);

				console.log('данные с сервера получены');
			} else {
				console.log('Ошибка! Что-то пошло не так!');
				console.log('Загрузка пустого проекта...');

				Object.assign(state.project,emptyData.project);
				state.pages.push(...emptyData.pages);
			}

			console.log(`house: ${idHouse}\nproject: ${idProject}`);
			console.log(res);
		},
	},
	actions:{
		// Вызов мутации в зависимости от ответа юзера в модальном окне
		mwUserConfirm({commit},confrim){
			let mwin = this.state.mwin;
			if(confrim){
				commit(mwin.action);
			}
			mwin.show=false;
		},
	}

});