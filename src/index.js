// JS
// import './js/';

// SCSS
import './assets/scss/main.scss';

// CSS (example)
// import './assets/css/main.css'

// Vue
import Vue from 'vue';
// window.Vue = require('vue');
import store from './store/store';

// Vue components global register (for use in html)
Vue.component('app', require('./components/app.vue').default);
// Vue.component('example', require('./components/example/example.vue').default);
// Vue.component('draw', require('./components/draw/draw.vue').default);
// import App from './components/app.vue';

// Vue init
new Vue({
	el: '#app',
	store,
	// render: h => h(App)
});

// CSS - libPlugins
// import '../node_modules/swiper/css/swiper.min.css';
// import '../node_modules/magnific-popup/dist/magnific-popup.css';